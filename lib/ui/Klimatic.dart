import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;

import 'package:klimatic/util/utils.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class Klimatic extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _Klimatic_state();
  }
}

class _Klimatic_state extends State<Klimatic> {
  String _cityentered;

  Future _GoToChangeCity(BuildContext) async {
    Map result = await Navigator.of(context).push(
        new MaterialPageRoute<Map>(builder: (prefix0.BuildContext context) {
      return new ChangeCity();
    }));
    if (result.containsKey('enter') && result != null) {
      _cityentered = result['enter'];
    } else {


    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text("Klimatic"),
        centerTitle: true,
        backgroundColor: Colors.redAccent,
        actions: <Widget>[
          new IconButton(
              icon: new Icon(Icons.menu),
              onPressed: () => _GoToChangeCity(context))
        ],
      ),
      body: new Stack(
        children: <Widget>[
          Center(
            child: Image.asset(
              "img/umbrella.png",
              width: 490.5,
              height: 1200.0,
              fit: BoxFit.fill,
            ),
          ),
          new Container(
            margin: EdgeInsets.fromLTRB(3, 15, 3, 1),
            alignment: Alignment.topCenter,
            child: Text(
              _cityentered == null ? defaultCity : _cityentered,
              style: citystyle(),
            ),
          ),
          new Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.fromLTRB(0, 200, 30, 0),
            child: UpdateTempWidget(_cityentered),
          )
        ],
      ),
    );
  }

  Future<Map> GetWeather(String apiId, String city) async {
    String apiurl =
        "https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$apiId&units=$unit";
    http.Response response = await http.get(apiurl);
    return json.decode(response.body);
    
  }

  Widget UpdateTempWidget(String temp) {
    return new FutureBuilder(
        future:
            GetWeather(apiId, temp == null ? temp = defaultCity : temp = temp),
        builder: (BuildContext context, AsyncSnapshot<Map> snapchat) {
          if (snapchat.hasData) {
            Map Content = snapchat.data;
            return new Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new ListTile(
                    title: Text(
                      "Temperature:${Content['main']['temp'].toString()} ${unit.compareTo("metric") == 0 ? "C" : "f"}.",
                      style: tempstyle(),
                    ),
                    subtitle: new ListTile(
                      title: new Text(
                        "Humidity:${Content['main']['humidity'].toString()} ."
                        "\nMin:${Content['main']['temp_min'].toString()}."
                        "\nMax:${Content['main']['temp_max'].toString()}.",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 23.5,
                            color: Colors.white70),
                      ),
                    ),
                  )
                ],
              ),
            );
          } else {
            return Container(
              child: Text("No internet Conection"),
            );
          }
        });
  }
}

TextStyle citystyle() {
  return TextStyle(
      fontSize: 25.5, color: Colors.white, fontStyle: FontStyle.italic);
}

TextStyle tempstyle() {
  return TextStyle(
      fontSize: 30, fontWeight: FontWeight.w600, color: Colors.white);
}

class ChangeCity extends StatefulWidget {
  @override
  _ChangeCityState createState() => _ChangeCityState();
}

class _ChangeCityState extends State<ChangeCity> {
  var _CityController = new TextEditingController();
  int radio_value = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.redAccent,
        centerTitle: true,
        title: Text("Change City "),
      ),
      body: new Stack(
        children: <Widget>[
          Image.asset(
            "img/white_snow.png",
            width: 500.0,
            height: 1200.0,
            fit: BoxFit.fill,
          ),
          new ListView(
            children: <Widget>[
              new ListTile(
                title: TextField(
                  decoration: new InputDecoration(labelText: "Enter City "),
                  controller: _CityController,
                  keyboardType: TextInputType.text,
                ),
              ),
              Center(
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Celues"),
                    Radio(
                        activeColor: Colors.redAccent.shade700,
                        value: 0,
                        groupValue: radio_value,
                        onChanged: _ChangeUnites),
                    Text("fharnite"),
                    Radio(
                        activeColor: Colors.orange,
                        value: 1,
                        groupValue: radio_value,
                        onChanged: _ChangeUnites)
                  ],
                ),
              ),
              new ListTile(
                title: new FlatButton(
                    onPressed: () {
                      Navigator.pop(context, {'enter': _CityController.text==""?defaultCity : _CityController.text});
                    },
                    color: Colors.redAccent,
                    textColor: Colors.white,
                    child: Text("Get weather ")),
              )
            ],
          )
        ],
      ),
    );
  }

  void _ChangeUnites(int value) {
    setState(() {
      radio_value = value;
      switch (value) {
        case 0:
          {
            unit = "metric";
            break;
          }

        case 1:
          {
            unit = "imperial";
            break;
          }
      }
    });
  }
}
